﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Skripsi_vijayps
{
    public partial class Form2 : Form
    {
        Koneksi Connection = new Koneksi();
        string SelectedItem, AccessRole;
        bool IsDGVNotEmpty;

        public Form2()
        {
            InitializeComponent();
            UpdateTable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Connection.RunQuery("INSERT INTO `menu` (`item`, `harga`, `kategori`) VALUES ('" + textBox1.Text + "', '" + textBox2.Text + "', '" + comboBox1.Text + "')");
            UpdateTable();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedItem = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            textBox1.Text = SelectedItem;
            textBox2.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            comboBox1.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
        }

        public void UpdateTable()
        {
            dataGridView1.DataSource = Connection.GetDataSet("SELECT item AS 'Item', harga AS 'Harga @', kategori AS 'Kategori' FROM menu").Tables[0];
            IsDGVNotEmpty = (dataGridView1.Rows.Count > 0);
            foreach (MenuItem Item in contextMenuStrip1.MenuItems) Item.Enabled = IsDGVNotEmpty;
            button2.Enabled = IsDGVNotEmpty;
            button3.Enabled = IsDGVNotEmpty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Connection.RunQuery("UPDATE `menu` SET `item` = '" + textBox1.Text + "', `harga` = '" + textBox2.Text + "', `kategori` = '" + comboBox1.Text + "' WHERE `menu`.`item` = '" + SelectedItem + "'");
            SelectedItem = textBox1.Text;
            UpdateTable();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus item " + SelectedItem, "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("DELETE FROM `menu` WHERE `menu`.`item` = '" + SelectedItem + "'");
                UpdateTable();
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                AccessRole = Form1.AccessRole;
                UpdateTable();
                comboBox1.DataSource = Connection.GetDataSet("SELECT kategori FROM kategori").Tables[0];
                comboBox1.DisplayMember = "kategori";
                toolStripButton2.Enabled = (comboBox1.Items.Count != 0);
                if (AccessRole == "A")
                {
                    toolStrip1.Visible = false;
                    dataGridView1.Dock = DockStyle.Fill;
                }
                else if (AccessRole == "P")
                {
                    label1.Visible = true;
                    label2.Visible = true;
                    label3.Visible = true;
                    textBox1.Visible = true;
                    textBox2.Visible = true;
                    comboBox1.Visible = true;
                    button1.Visible = true;
                    button2.Visible = true;
                    button3.Visible = true;
                }
                else if (AccessRole == "K")
                {
                    toolStrip1.Visible = false;
                    dataGridView1.Dock = DockStyle.Fill;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            comboBox1.DropDownStyle = ComboBoxStyle.DropDown;
            comboBox1.Select();
        }

        private void comboBox1_Leave(object sender, EventArgs e)
        {
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Enter && comboBox1.DropDownStyle == ComboBoxStyle.DropDown)
                {
                    Connection.RunQuery("INSERT INTO `kategori` (`kategori`) VALUES ('" + comboBox1.Text + "')");
                    comboBox1.DataSource = Connection.GetDataSet("SELECT kategori FROM kategori").Tables[0];
                    comboBox1.DisplayMember = "kategori";
                    comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                    toolStripButton2.Enabled = true;
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus kategori " + comboBox1.Text, "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("DELETE FROM `kategori` WHERE `kategori`.`kategori` = \'" + comboBox1.Text + "\'");
                comboBox1.DataSource = Connection.GetDataSet("SELECT kategori FROM kategori").Tables[0];
                comboBox1.DisplayMember = "kategori";
                toolStripButton2.Enabled = (comboBox1.Items.Count != 0);
            }
        }

        private void hapusSemuaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus semua menu", "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("TRUNCATE `menu`");
                UpdateTable();
            }
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) contextMenuStrip1.Show(dataGridView1, new Point(e.X, e.Y));
        }
    }
}
