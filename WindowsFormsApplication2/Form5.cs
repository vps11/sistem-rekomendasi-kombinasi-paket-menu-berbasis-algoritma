﻿using System;
using System.Windows.Forms;

namespace Skripsi_vijayps
{
    public partial class Form5 : Form
    {
        Koneksi Connection = new Koneksi();
        string SelectedItem;
        bool IsDGVNotEmpty;

        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            comboBox1.Text = "Karyawan - K";
            UpdateTable();
        }

        public void UpdateTable()
        {
            dataGridView1.DataSource = Connection.GetDataSet("SELECT id AS 'No.', nama AS 'Nama', hakakses AS 'Hak Akses' FROM pengguna").Tables[0];
            IsDGVNotEmpty = (dataGridView1.Rows.Count != 0);
            button2.Enabled = IsDGVNotEmpty;
            button3.Enabled = IsDGVNotEmpty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Connection.RunQuery("INSERT INTO `pengguna` (`nama`, `password`, `hakakses`) VALUES ('" + textBox1.Text + "', MD5('" + textBox2.Text + "'), '" + comboBox1.Text.Substring(comboBox1.Text.Length - 1) + "')");
            UpdateTable();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Length > 0)
            {
                Connection.RunQuery("UPDATE `pengguna` SET `nama` = '" + textBox1.Text + "', `password` = MD5('" + textBox2.Text + "'), `hakakses` = '" + comboBox1.Text.Substring(comboBox1.Text.Length - 1) + "' WHERE `pengguna`.`nama` = '" + SelectedItem + "'");
                SelectedItem = textBox1.Text;
                UpdateTable();
            }
            else
            {
                MessageBox.Show("Silahkan isi passwordnya.");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedItem = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            textBox1.Text = SelectedItem;
            textBox2.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus pengguna " + SelectedItem, "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("DELETE FROM `pengguna` WHERE `pengguna`.`nama` = '" + SelectedItem + "'");
                UpdateTable();
            }
        }
    }
}
