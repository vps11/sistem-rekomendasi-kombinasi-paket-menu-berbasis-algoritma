"""
Sumber dengan perubahan seperlunya:
https://github.com/asaini/Apriori
"""
import sys

from itertools import chain, combinations
from collections import defaultdict
from optparse import OptionParser

def return_items_with_min_support(itemset, transaction_list, min_support, frequent_set):
    """
Menghitung support untuk setiap item dalam itemset dan mengembalikan sebuah subset atau himpunan bagian dari masing-masing itemset yang memenuhi minimum support.
    """
    _itemset = set()
    local_set = defaultdict(int)
    for item in itemset:
        for transaction in transaction_list:
            if item.issubset(transaction):
                frequent_set[item] += 1
                local_set[item] += 1
    for item, count in local_set.items():
        support = float(count) / len(transaction_list)
        if support >= min_support:
            _itemset.add(item)
    return _itemset

def get_itemset_transaction_list(data_iterator):
    """
Mengembalikan itemset dan daftar transaksi dari data yang ada.
    """
    transaction_list = list()
    itemset = set()
    for record in data_iterator:
        transaction = frozenset(record)
        transaction_list.append(transaction)
        for item in transaction:
            itemset.add(frozenset([item])) # membentuk 1 itemset
    return itemset, transaction_list

def run_apriori(data_iterator, min_support, min_confidence):
    """
Mengembalikan:
- items: (tuple, support)
- rules: ((pretuple, posttuple), confidence, lift)
    """
    itemset, transaction_list = get_itemset_transaction_list(data_iterator)
    frequent_set = defaultdict(int)
    large_set = dict()
    current_large_set = return_items_with_min_support(itemset, transaction_list, min_support, frequent_set)
    k = 2
    while(current_large_set != set([])):
        large_set[k - 1] = current_large_set
        current_large_set = set([i.union(j) for i in current_large_set for j in current_large_set if len(i.union(j)) == k])
        current_large_set = return_items_with_min_support(current_large_set, transaction_list, min_support, frequent_set)
        k += 1
        
    def get_support(item):
        return float(frequent_set[item]) / len(transaction_list)

    return_items = []
    for key, value in large_set.items():
        return_items.extend([(tuple(item), get_support(item)) for item in value])
    return_rules = []
    for key, value in large_set.items()[1:]:
        for item in value:
            for element in map(frozenset, [x for x in chain(*[combinations(item, i + 1) for i, a in enumerate(item)])]):
                remain = item.difference(element)
                if len(remain) > 0:
                    confidence = get_support(item) / get_support(element) # C = S(X -> Y) / S(X)
                    if confidence >= min_confidence:
                        lift = confidence / get_support(remain)           # L = C / S(Y)
                        return_rules.append(((tuple(element), tuple(remain)), confidence, lift))
    print_results(return_items, return_rules)

def print_results(items, rules):
    """
Cetak hasil ke layar.
    """
    #for item, support in sorted(items, key = lambda(item, support): support, reverse = True):
    #    print "%s, support = %.2f%%" % (str(item), support * 100)
    print "Rekomendasi kombinasi paket menu yang dihasilkan:"
    for rule, confidence, lift in sorted(rules, key = lambda(rule, confidence, lift): lift, reverse = True):
        pre, post = rule
        print "%s --> %s, conf. = %.2f%%, lift = %.2f" % (str(pre), str(post), confidence * 100, lift)

def data_from_file(file):
    """
Mengambil data dari file.
    """
    file_iteration = open(file, 'rU')
    for line in file_iteration:
        line = line.strip().rstrip(',')
        record = frozenset(line.split(','))
        yield record

if __name__ == "__main__":
    option_parser = OptionParser()
    option_parser.add_option('-f', '--inputFile', dest = 'input', help = 'file transaksi CSV', default = None)
    option_parser.add_option('-s', '--minSupport', dest = 'min_sup', help = 'minimum support dalam %', default = 10, type = 'float')
    option_parser.add_option('-c', '--minConfidence', dest = 'min_conf', help = 'minimum confidence dalam %', default = 10, type = 'float')
    (options, args) = option_parser.parse_args()
    if options.input is None: data = sys.stdin
    elif options.input is not None: data = data_from_file(options.input)
    else:
        print 'Dataset tidak ditentukan.'
        sys.exit()
    run_apriori(data, options.min_sup / float(100), options.min_conf / float(100))