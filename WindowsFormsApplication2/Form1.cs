﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Skripsi_vijayps
{
    public partial class Form1 : Form
    {
        Koneksi Connection = new Koneksi();
        Form2 MenuForm = new Form2();
        Form3 ResultForm;
        Form4 LoginForm = new Form4();
        Form5 UserForm = new Form5();
        bool IsTableNotEmpty;
        int ComboBoxItemCount = 1, TextBoxItemCount = 3;
        string Items, Quantities;
        public static bool IsLoggedIn;
        public static string Username, AccessRole;
        public static int Userid;

        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MenuForm.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Tambah data transaksi", "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Items = null;
                Quantities = null;
                for (int i = 1; i <= ComboBoxItemCount; i++)
                {
                    foreach (Control Item in panel1.Controls)
                    {
                        if (i < ComboBoxItemCount)
                        {
                            if (Item.Name == "comboBox" + i) Items += Item.Text + ",";
                        }
                        else if (Item.Name == "comboBox" + ComboBoxItemCount) Items += Item.Text;
                    }
                }
                for (int i = 3; i <= TextBoxItemCount; i++)
                {
                    foreach (Control Quantity in panel1.Controls)
                    {
                        if (i < TextBoxItemCount)
                        {
                            if (Quantity.Name == "textBox" + i) Quantities += Quantity.Text + ",";
                        }
                        else if (Quantity.Name == "textBox" + TextBoxItemCount) Quantities += Quantity.Text;
                    }
                }
                Connection.RunQuery("INSERT INTO `transaksi` (`itemset`, `banyaknya`, `waktu`, `idpenjual`) VALUES ('" + Items + "', '" + Quantities + "', '" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm") + "', '" + Userid + "')");
                UpdateTable();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus transaksi penjualan no. " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString(), "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("DELETE FROM `transaksi` WHERE `transaksi`.`id` = " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
                UpdateTable();
            }
        }

        private void UpdateTable()
        {
            dataGridView1.DataSource = Connection.GetDataSet("SELECT id AS 'No.', itemset AS 'Itemset', banyaknya AS 'Jumlah per Item', waktu AS 'Tanggal, Waktu', idpenjual AS 'No. Penjual' FROM transaksi WHERE `waktu` BETWEEN '" + dateTimePicker2.Value.ToString("yyyy-MM-dd") + "' AND '" + dateTimePicker3.Value.ToString("yyyy-MM-dd") + "'").Tables[0];
            IsTableNotEmpty = (dataGridView1.Rows.Count != 0);
            foreach (MenuItem Item in contextMenuStrip1.MenuItems) Item.Enabled = IsTableNotEmpty;
            button3.Enabled = IsTableNotEmpty;
            button5.Enabled = IsTableNotEmpty;
            dataGridView1.Rows[dataGridView1.Rows.Count - 1].Selected = true;
            dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.Rows.Count - 1;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ComboBox ComboBoxItem = new ComboBox();
            TextBox TextBoxItem = new TextBox();
            ComboBoxItem.DataSource = Connection.GetDataSet("SELECT item FROM menu").Tables[0];
            ComboBoxItem.DisplayMember = "item";
            ComboBoxItem.DropDownStyle = ComboBoxStyle.DropDownList;
            ComboBoxItem.FormattingEnabled = true;
            ComboBoxItem.Location = new Point(3, comboBox1.Location.Y + (27 * ComboBoxItemCount));
            ComboBoxItem.Name = "comboBox" + ++ComboBoxItemCount;
            ComboBoxItem.Size = new Size(comboBox1.Width, comboBox1.Height);
            ComboBoxItem.SelectionChangeCommitted += new EventHandler(comboBox1_SelectionChangeCommitted);
            panel1.Controls.Add(ComboBoxItem);
            TextBoxItem.Location = new Point(183, textBox3.Location.Y + (27 * (TextBoxItemCount - 2)));
            TextBoxItem.Name = "textBox" + ++TextBoxItemCount;
            TextBoxItem.Size = new Size(textBox3.Width, textBox3.Height);
            TextBoxItem.Text = "1";
            panel1.Controls.Add(TextBoxItem);
            toolStripButton2.Enabled = true;
            panel1.VerticalScroll.Value = panel1.VerticalScroll.Maximum;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            foreach (Control Item in panel1.Controls)
            {
                if (Item.Name == "comboBox" + ComboBoxItemCount)
                {
                    panel1.Controls.Remove(Item);
                    ComboBoxItemCount--;
                    if (ComboBoxItemCount == 1) toolStripButton2.Enabled = false;
                    break;
                }
            }
            foreach (Control Item in panel1.Controls)
            {
                if (Item.Name == "textBox" + TextBoxItemCount)
                {
                    panel1.Controls.Remove(Item);
                    TextBoxItemCount--;
                    break;
                }
            }
        }

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) contextMenuStrip1.Show(dataGridView1, new Point(e.X, e.Y));
        }

        private void hapusSemuaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus semua dan reset penomoran", "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("TRUNCATE `transaksi`");
                UpdateTable();
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridView1.SelectedRows[0].Cells[0].Value.ToString() + ";" + dataGridView1.SelectedRows[0].Cells[1].Value.ToString() + ";" + dataGridView1.SelectedRows[0].Cells[2].Value.ToString());
        }

        private void copyItemsetTerpilihToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(dataGridView1.SelectedRows[0].Cells[1].Value.ToString());
        }

        private void CSV_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ExportFileDialog = new SaveFileDialog();
                ExportFileDialog.Filter = "CSV (*.csv)|*.csv|Semua File (*.*)|*.*";
                ExportFileDialog.Title = "Ekspor Itemset Terpilih ke CSV";
                if (ExportFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter Writer = new StreamWriter(ExportFileDialog.FileName);
                    CSVTransactionWriter(Writer);
                }
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            foreach (Control Item in panel1.Controls)
            {
                if (Item.Name.Contains("comboBox"))
                {
                    ((ComboBox)Item).DataSource = Connection.GetDataSet("SELECT item FROM menu").Tables[0];
                    ((ComboBox)Item).DisplayMember = "item";
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            StreamWriter Writer = new StreamWriter("tes.csv");
            CSVTransactionWriter(Writer);
            ResultForm = new Form3(textBox1.Text, textBox2.Text);
            ResultForm.ShowDialog();
        }

        private void SPMF_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ExportFileDialog = new SaveFileDialog();
                ExportFileDialog.Filter = "SPMF (*.txt)|*.txt|Semua File (*.*)|*.*";
                ExportFileDialog.Title = "Ekspor Transaksi Terpilih ke SPMF";
                if (ExportFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter Writer = new StreamWriter(ExportFileDialog.FileName);
                    string SPMF = null;
                    string[] Menu = Connection.GetDataStrings("SELECT `item` FROM `menu`");
                    foreach (DataGridViewRow Row in dataGridView1.Rows) SPMF += "'" + Row.Cells[1].Value.ToString().Replace(",", "','") + "'" + Environment.NewLine;
                    for (int i = 0; i < Menu.Length; i++) SPMF = SPMF.Replace("'" + Menu[i] + "'", (i + 1).ToString());
                    SPMF = SPMF.Replace(',', ' ');
                    Writer.Write(SPMF);
                    Writer.Close();
                }
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            UserForm.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ResultForm = new Form3();
            ResultForm.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ExportFileDialog = new SaveFileDialog();
                ExportFileDialog.Filter = "Excel (*.xls)|*.xls|HTML (*.htm)|*.htm|Semua File (*.*)|*.*";
                ExportFileDialog.Title = "Cetak Laporan Transaksi";
                if (ExportFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter Writer = new StreamWriter(ExportFileDialog.FileName);
                    string XLS = "Laporan Transaksi Penjualan Wedangan Solo<br><hr>Dicetak " + DateTime.Now;
                    XLS += "<br><br><table border='1'><thead><td>No.</td><td>Barang</td><td>Jumlah per Barang</td><td>Tanggal, Waktu</td><td>Id Penjual</td></thead><tbody>";
                    foreach (DataGridViewRow Row in dataGridView1.Rows)
                    {
                        XLS += "<tr>";
                        for (int i = 0; i < Row.Cells.Count; i++)
                        {
                            if (i == 1 || i == 2) XLS += "<td>" + Row.Cells[i].Value.ToString().Replace(",", "<br>") + "</td>";
                            else XLS += "<td>" + Row.Cells[i].Value.ToString() + "</td>";
                        }
                        XLS += "</tr>";
                    }
                    Writer.Write(XLS += "</tbody></table>");
                    Writer.Close();
                }
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
        }

        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            foreach (Control Item in panel1.Controls)
            {
                if (Item.Name == "textBox" + (int.Parse(((ComboBox)sender).Name.Replace("comboBox", "")) + 2))
                {
                    ((TextBox)Item).SelectAll();
                    ((TextBox)Item).Focus();
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string Message = "Support (nilai penunjang, maks. 100%) adalah persentase kombinasi suatu item dalam data transaksi yang dipilih.";
            Message += " Confidence (nilai kepastian, maks. 100%) adalah kuatnya hubungan antar item dalam data tersebut.";
            Message += " Jika jumlah rekomendasi yang dihasilkan kurang atau tidak ada, anda bisa menurunkan nilai minimum support atau confidence.";
            Message += " Rekomendasi yang dihasilkan bergantung dengan nilai minimum support dan confidence yang dimasukkan.";
            MessageBox.Show(Message);
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ExportFileDialog = new SaveFileDialog();
                ExportFileDialog.Filter = "Excel (*.xls)|*.xls|Semua File (*.*)|*.*";
                ExportFileDialog.Title = "Ekspor Transaksi Terpilih ke Excel untuk Perhitungan Manual 2-Itemset";
                if (ExportFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter Writer = new StreamWriter(ExportFileDialog.FileName);
                    string XLS = "<table border='1'><thead><td><b>Transaksi</b></td>";
                    string[] Menu = Connection.GetDataStrings("SELECT `item` FROM `menu`");
                    string[] BtoDC = AlphabetArray(Menu.Length + 1);
                    string ExcelLastTRow = (dataGridView1.Rows.Count + 1).ToString();
                    for (int i = 1; i <= Menu.Length; i++) XLS += "<td>" + i.ToString() + "</td>";
                    XLS += "</thead><tbody>";
                    foreach (DataGridViewRow Row in dataGridView1.Rows)
                    {
                        string Transaction = "'" + Row.Cells[1].Value.ToString().Replace(",", "','") + "'";
                        XLS += "<tr><td></td>";
                        for (int i = 1; i <= Menu.Length; i++)
                        {
                            if (Transaction.Contains("'" + Menu[i - 1] + "'")) XLS += "<td>1</td>";
                            else XLS += "<td></td>";
                        }
                        XLS += "</tr>";
                    }
                    XLS += "</tbody></table>Jumlah kombinasi 2-itemset<br>=COMBIN(" + Menu.Length + ";2)<br><br><table border='1'><thead><td>2-itemset</td>";
                    for (int i = 1; i <= Menu.Length; i++)
                    {
                        if (i == 1) XLS += "<td>--></td>";
                        else XLS += "<td>" + i.ToString() + "</td>";
                    }
                    XLS += "</thead><tbody>";
                    for (int i = 1; i < Menu.Length; i++)
                    {
                        XLS += "<tr><td></td><td>" + i.ToString() + "</td>";
                        for (int j = 1; j < Menu.Length; j++)
                        {
                            if (j < i) XLS += "<td></td>";
                            else XLS += "<td>=COUNTIFS(" + BtoDC[i] + "2:" + BtoDC[i] + ExcelLastTRow + ";1;" + BtoDC[j + 1] + "2:" + BtoDC[j + 1] + ExcelLastTRow + ";1)</td>";
                        }
                    }
                    XLS += "</tbody></table>Support tertinggi";
                    Writer.Write(XLS);
                    Writer.Close();
                }
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (IsLoggedIn)
            {
                string ARName;
                dateTimePicker2.Value = new DateTime(2018, 4, 1);
                dateTimePicker3.Value = DateTime.Now.AddDays(1);
                UpdateTable();
                comboBox1.DataSource = Connection.GetDataSet("SELECT item FROM menu").Tables[0];
                comboBox1.DisplayMember = "item";
                if (AccessRole == "A")
                {
                    ARName = "Admin";
                    button7.Visible = true;
                    button1.Visible = false;
                    button5.Visible = false;
                    panel1.Visible = false;
                    toolStrip1.Visible = false;
                    dateTimePicker1.Visible = false;
                    button8.Visible = false;
                }
                else if (AccessRole == "P")
                {
                    ARName = "Pemilik";
                    button3.Visible = true;
                    button9.Visible = true;
                    groupBox1.Visible = true;
                }
                else
                {
                    ARName = "Karyawan";
                }
                label5.Text = Username + " (" + ARName + ")";
            }
            else
            {
                Hide();
                LoginForm.ShowDialog();
                Form1_Load(sender, e);
            }
        }

        private void CSVTransactionWriter(StreamWriter Writer)
        {
            string CSV = null;
            foreach (DataGridViewRow Row in dataGridView1.Rows) CSV += Row.Cells[1].Value.ToString() + Environment.NewLine;
            Writer.Write(CSV);
            Writer.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            UpdateTable();
        }

        /// <summary>
        /// Menghasilkan array berisi urutan abjad A-Z, dilanjutkan dengan AA-AZ dst. Item[0] huruf A.
        /// </summary>
        /// <param name="LastIndex">2 = B, 107 = DC</param>
        /// <returns></returns>
        public static string[] AlphabetArray(int LastIndex)
        {
            List<string> Alphabets = new List<string>();
            for (int i = 1; i <= LastIndex; i++)
            {
                string CurrentAlphabet = "";
                int Index = i;
                while (Index > 0)
                {
                    int CAIndex = (Index - 1) % 26;
                    CurrentAlphabet = (char)(CAIndex + 65) + CurrentAlphabet;
                    Index = (Index - (CAIndex + 1)) / 26;
                }
                Alphabets.Add(CurrentAlphabet);
            }
            return Alphabets.ToArray();
        }
    }
}
