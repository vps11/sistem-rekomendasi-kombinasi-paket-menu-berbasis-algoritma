﻿using System;
using System.Windows.Forms;

namespace Skripsi_vijayps
{
    public partial class Form4 : Form
    {
        Koneksi Connection = new Koneksi();
        string AccessRole;
        int Userid;

        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string SqlWhere = "WHERE `nama` = '" + textBox1.Text + "' AND `password` = MD5('" + textBox2.Text + "')";
            string[] UserCheck = Connection.GetDataStrings("SELECT `id` FROM `pengguna` " + SqlWhere);
            if (UserCheck.Length > 0)
            {
                AccessRole = Connection.GetDataStrings("SELECT `hakakses` FROM `pengguna` " + SqlWhere)[0];
                Userid = int.Parse(UserCheck[0]);
                if (Userid > 0)
                {
                    Form1.AccessRole = AccessRole;
                    Form1.Username = textBox1.Text;
                    Form1.Userid = Userid;
                    Form1.IsLoggedIn = true;
                    Close();
                }
            }
            else
            {
                MessageBox.Show("Username dan password tidak cocok.");
            }
        }
    }
}
