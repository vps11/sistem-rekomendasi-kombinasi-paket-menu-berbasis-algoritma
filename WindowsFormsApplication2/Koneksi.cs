﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace Skripsi_vijayps
{
    class Koneksi : IDisposable
    {
        protected MySqlConnection Connect = new MySqlConnection("datasource=localhost;port=3306;username=root;password=;database=skripsi_vijayps");
        MySqlCommand Command;
        MySqlDataAdapter Adapter;
        MySqlDataReader Reader;

        public DataSet GetDataSet(string Query)
        {
            DataSet DataSet = new DataSet();
            try
            {
                Connect.Open();
                Adapter = new MySqlDataAdapter(Query, Connect);
                Adapter.Fill(DataSet);
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
                DataSet.Tables.Add();
            }
            finally
            {
                Connect.Close();
            }
            return DataSet;
        }

        public string[] GetDataStrings(string Query)
        {
            List<string> Read = new List<string>();
            try
            {
                Connect.Open();
                Command = new MySqlCommand(Query, Connect);
                Reader = Command.ExecuteReader();
                while (Reader.Read()) Read.Add(Reader.GetString(0));
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
            finally
            {
                Connect.Close();
            }
            return Read.ToArray();
        }

        public void RunQuery(string Query)
        {
            try
            {
                Connect.Open();
                Command = new MySqlCommand(Query, Connect);
                Command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Connect.Close();
            }
        }

        public void Dispose()
        {
            Connect.Dispose();
            Command.Dispose();
            Adapter.Dispose();
            Reader.Close();
        }
    }
}
