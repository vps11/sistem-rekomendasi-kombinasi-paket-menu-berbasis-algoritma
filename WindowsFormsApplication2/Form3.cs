﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Skripsi_vijayps
{
    public partial class Form3 : Form
    {
        string Result, SelectedItem;
        Koneksi Connection = new Koneksi();
        bool IsListNotEmpty;

        public Form3()
        {
            InitializeComponent();
        }

        public Form3(string MinSup, string MinConf)
        {
            MinSup = MinSup.Replace(',', '.');
            MinConf = MinConf.Replace(',', '.');
            ProcessStartInfo ProcessStart = new ProcessStartInfo();
            ProcessStart.FileName = "ipy.exe";
            ProcessStart.Arguments = string.Format("apriori.py -f tes.csv -s " + MinSup + " -c " + MinConf);
            ProcessStart.CreateNoWindow = true;
            ProcessStart.UseShellExecute = false;
            ProcessStart.RedirectStandardOutput = true;
            Process Process = Process.Start(ProcessStart);
            StreamReader Reader = Process.StandardOutput;
            InitializeComponent();
            Result = Reader.ReadToEnd().Replace("'", "").Replace(",)", ")");
            textBox1.Text = Result;
            Connection.RunQuery("INSERT INTO `rekomendasi` (`hasil`, `minsup`, `minconf`, `waktu`) VALUES ('" + Result + "', '" + MinSup + "', '" + MinConf + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')");
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            UpdateList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog ExportFileDialog = new SaveFileDialog();
                ExportFileDialog.Filter = "Excel (*.xls)|*.xls|HTML (*.htm)|*.htm|Semua File (*.*)|*.*";
                ExportFileDialog.Title = "Cetak Laporan Rekomendasi";
                if (ExportFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter Writer = new StreamWriter(ExportFileDialog.FileName);
                    string XLS = "Laporan Rekomendasi Kafe Wedangan Solo<br><hr>Dicetak " + DateTime.Now;
                    XLS += Result.Replace("\r\n", "<br>");
                    Writer.Write(XLS);
                    Writer.Close();
                }
            }
            catch (Exception Err)
            {
                MessageBox.Show(Err.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Hapus rekomendasi tanggal " + SelectedItem, "Anda Yakin?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Connection.RunQuery("DELETE FROM `rekomendasi` WHERE `rekomendasi`.`waktu` = '" + DateTime.Parse(SelectedItem).ToString("yyyy-MM-dd HH:mm:ss") + "'");
                UpdateList();
            }
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            if (listBox1.Text != SelectedItem && listBox1.Items.Count > 0)
            {
                SelectedItem = listBox1.Text;
                if (SelectedItem.Length > 0)
                {
                    Result = Connection.GetDataStrings("SELECT `hasil` FROM `rekomendasi` WHERE `waktu` = '" + DateTime.Parse(SelectedItem).ToString("yyyy-MM-dd HH:mm:ss") + "'")[0];
                    textBox1.Text = Result;
                    button2.Enabled = true;
                    button3.Enabled = true;
                }
            }
        }

        private void UpdateList()
        {
            listBox1.DataSource = Connection.GetDataSet("SELECT `waktu` FROM `rekomendasi`").Tables[0];
            listBox1.DisplayMember = "waktu";
            listBox1.ClearSelected();
            IsListNotEmpty = (listBox1.SelectedItems.Count > 0);
            button2.Enabled = IsListNotEmpty;
            button3.Enabled = IsListNotEmpty;
        }
    }
}
